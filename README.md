# README #

This is the code used for the 2021 Phil Trans A paper "The impact of spatially varying wetland source signatures on the atmospheric variability of dD-CH4".

### How to create the dD-CH4 map ###
1. Download the dD-H2O map from https://wateriso.utah.edu/waterisotopes/pages/data_access/ArcGrids.html into a folder called "d2h_ch4" - you only need Hma.asc
2. Create dD-CH4 map by running d2h_ch4_map.py (you'll have to set DATA_DIR to where your d2h_ch4 folder is)

### Who do I talk to? ###

If you do run into problems, feel free to contact us at anita.ganesan@bristol.ac.uk.