#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to create wetland emissions D2H value maps.

Author: Angharad Stell (angharadstell@gmail.com)
"""
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr

DATA_DIR = Path("/work/as16992")

def sanity_plot(data, lon, lat, nlabel):
    """ Plot a map to check it looks sensible. """
    if lat[0] < lat[-1]:
        lat = lat[::-1]
        data = data[::-1, :]
    
    plt.imshow(data, cmap="Reds")
    plt.xticks(np.arange(0, len(lon), nlabel),
               lon[::nlabel].astype(int))
    plt.yticks(np.arange(0, len(lat), int(nlabel/2)),
               lat[::int(nlabel/2)].astype(int))
    plt.colorbar()
    plt.show()
    plt.close()

if __name__ == "__main__":
    # Read in d2h from precipitation
    # https://wateriso.utah.edu/waterisotopes/pages/data_access/ArcGrids.html
    ppt_d2h = np.genfromtxt(DATA_DIR / "d2h_ch4/Hma.asc", 
                            skip_header=6)
    
    # Replace missing values with nan
    ppt_d2h[ppt_d2h == -9999.] = np.nan
    
    # Reshape to (2083, 4320)
    ncol = 4320
    nrow = 2083
    ppt_d2h_reshape = np.reshape(ppt_d2h, (nrow, ncol))
    
    
    # This is a silly file format that seems to assume the grid is square,
    # and the top rows of nans are cut off
    filler_array = np.zeros((int(ncol/2 - nrow), ncol))
    filler_array[:,:] = np.nan
    ppt_d2h_reshape = np.concatenate((filler_array,
                                      ppt_d2h_reshape))
    
    # Convert to d2h-ch4
    # https://bg.copernicus.org/preprints/bg-2020-410/bg-2020-410.pdf
    ch4_d2h = 0.62949 * ppt_d2h_reshape - 279.07
    
    # Lat and lon of file
    lon = np.linspace(-180, 180, ncol, endpoint=False)
    lat = np.linspace(-90, 90, int(ncol/2), endpoint=False)
    
    # Create nice xarray object
    # Have to reverse lat as llcorner is (-180, -90)
    # Transform to gridcell centre
    lon_cell_width = lon[1] - lon[0]
    lat_cell_width =  lat[1] - lat[0]
    xr_lon = lon + lon_cell_width / 2
    xr_lat = lat[::-1] + lat_cell_width / 2
    
    
    # Check ppt_d2h makes sense
    sanity_plot(ppt_d2h_reshape, xr_lon, xr_lat, 1000)
    
    # Check d2h ch4 ems look sensible
    sanity_plot(ch4_d2h, xr_lon, xr_lat, 1000)
    
    ch4_d2h_xr = xr.Dataset({"ch4_d2h_varying":(["lat", "lon"], ch4_d2h)},
                            {"lat":xr_lat, "lon":xr_lon})
    
    # Save d2h map
    ch4_d2h_xr.to_netcdf(DATA_DIR / "d2h_ch4/d2h_ch4_map.nc")
